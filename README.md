# nfc-spotify-controller

A NFC Sonos-Spotify controller for Raspberry PI's etc

This code was based upon and shamelessly inspired by the great QRocodile project.

This also has code to control a MFRC522 compatible light strip via Python and a Philips Hue light in the room.

## What Is It?

Code to control your Sonos speakers and Spotify songs via NFC tags via something like a Raspberry PI. 

## Installation and Setup

### 1. Prepare your Raspberry Pi

One one Raspberry PI, clone this repository.

For the light strip you also need to install the [neopixel drivers](https://github.com/jgarff/rpi_ws281x)

To start on boot there is a start script in the repo called start-detector.sh

You can install it with the following:

    systemctl edit --force --full detector.service

And fill the contents with something like:
    
    [Unit]
    Description=My Script Service
    Wants=network-online.target
    After=network-online.target
    
    [Service]
    Type=simple
    User=root
    WorkingDirectory=/root
    ExecStart=/usr/local/bin/start-detector.sh
    
    [Install]
    WantedBy=multi-user.target

### 2. Start `node-sonos-http-api`

Currently `nfc-spotify-controller` relies on a [custom fork](https://github.com/chrispcampbell/node-sonos-http-api/tree/qrocodile) of `node-sonos-http-api` that has been modified to do things like:

* look up library tracks using only a hash string (to keep the QR codes simple)
* return a list of all available library tracks and their associated hashes
* speak the current/next track
* play the album associated with a song
* other things I'm forgetting at the moment

(Note: `node-sonos-http-api` made it easy to bootstrap this project, as it already did much of what I needed.  However, it would probably make more sense to use something like [SoCo](https://github.com/SoCo/SoCo) (a Sonos controller API for Python) so that we don't need to run a separate server, and `qrplay` could control the Sonos system directly.)

It's possible to run `node-sonos-http-api` directly on the Raspberry Pi, so that you don't need an extra machine running, but I found that it's kind of slow this way (especially when the QR scanner process is already taxing the CPU), so I usually have it running on a separate machine to keep things snappy.

To install, clone my fork, check out the `qrocodile` branch, install, and start:

```
% git clone -b qrocodile https://github.com/chrispcampbell/node-sonos-http-api.git
% cd node-sonos-http-api
% npm install --production
% npm start
```

### 3. Generate some cards with `SimpleMFRC522.py`

To create some NFC tags you can use SimpleMFRC522 which is available [here](https://github.com/pimylifeup/MFRC522-python)

and a simple write script like the one included in this repo.

You can get Spotify Albums and Songs easily via the URL in play.spotify.com. Currently this script doesn't support playlists
because Spotify treats them slightly differently.


### 4. Changing Hue Light Colors

The Hue light driver uses the Ehabian interface as a RESTful driver for the Hue lights. It takes the light name to 
construct the url, and an RGB color to set the light color. 


### 5. Todo

Lots of stuff


## Acknowledgments

* [The Original QRocodile](https://github.com/chrispcampbell/qrocodile)
* [node-sonos-http-api](https://github.com/jishi/node-sonos-http-api)
* [spotipy](https://github.com/plamere/spotipy)


## License

`nfc-spotify-controller` is released under an MIT license. See the LICENSE file for the full license.

import time
from neopixel import *
import threading
import urllib2
import random
import time
# LED strip configuration:
LED_COUNT      = 60      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53


class Lights(threading.Thread):
    def __init__(self):
        super(Lights, self).__init__()
        self._stop_event = threading.Event()
        self.loop = True
        self.miniloop = True
        self.stack = []
        self.strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
        self.strip.begin()
        self.light_party = False
        self.light_party_url = ""

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        self.colorWipe(self.strip, Color(0,0,0), 10)
        return self._stop_event.is_set()

    def wheel(self, pos):
        """Generate rainbow colors across 0-255 positions."""
        if pos < 85:
            return Color(pos * 3, 255 - pos * 3, 0)
        elif pos < 170:
            pos -= 85
            return Color(255 - pos * 3, 0, pos * 3)
        else:
            pos -= 170
            return Color(0, pos * 3, 255 - pos * 3)

    def rainbow(self, strip, wait_ms=20, iterations=1):
        """Draw rainbow that fades across all pixels at once."""
        for j in range(256*iterations):
            if self.miniloop:
              for i in range(strip.numPixels()):
                strip.setPixelColor(i, self.wheel((i+j) & 255))
              strip.show()
              time.sleep(wait_ms/1000.0)
            else:
                break

    def rainbowCycle(self, strip, wait_ms=20, iterations=5):
        """Draw rainbow that uniformly distributes itself across all pixels."""
        for j in range(256*iterations):
            for i in range(strip.numPixels()):
                strip.setPixelColor(i, self.wheel((int(i * 256 / strip.numPixels()) + j) & 255))
            strip.show()
            time.sleep(wait_ms/1000.0)

    # Define functions which animate LEDs in various ways.
    def colorWipe(self, strip, color, wait_ms=50):
        """Wipe color across display a pixel at a time."""
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, color)
            strip.show()
            time.sleep(wait_ms/1000.0)

    def red(self):
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, Color(0, 255, 0))
            self.strip.show()
            time.sleep(10/1000.0)

    def green(self):
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, Color(255, 0, 0))
            self.strip.show()
            time.sleep(50/1000.0)

    def set_loop(self, v):
        self.loop = v

    def set_mini_loop(self, v):
        self.miniloop = v

    def add_instruction(self, inst):
        self.stack.append(inst)
        self.set_mini_loop(False)

    def start_lightparty(self, url):
      self.light_party = True
      self.light_party_url = url

    def stop_lightparty(self):
        self.light_party = False

    def perform_request(self, url, data=None, type="GET"):
      print(url)
      if(type=="GET"):
        response = urllib2.urlopen(url)
        result = response.read()
        print(result)
      elif type=="POST":
        handler=urllib2.HTTPHandler(debuglevel=1)
        opener = urllib2.build_opener(handler)
        urllib2.install_opener(opener)
        request = urllib2.Request(url, data, headers={"Accept":"application/json", "Content-Type": "text/plain"})
        try:
            response = urllib2.urlopen(request)
            result = response.read()
            print(result)
        except urllib2.HTTPError as e:
            error_message = e.read()
            print error_message
            pass

    def update_light(self):
        hue = random.randint(1,300)
        sat = random.randint(1,101)
        data = str(hue)+", "+str(sat)+", 80"
        self.perform_request(self.light_party_url, data, "POST")

    def run(self):
        print ('Rainbow animations.')
        while self.loop:
            if self.light_party:
                ts = time.gmtime()
                if int(time.strftime("%S", ts)) < 10 or (int(time.strftime("%S", ts)) >15 and int(time.strftime("%S", ts)) < 20) \
                        or (int(time.strftime("%S", ts)) >30 and int(time.strftime("%S", ts)) < 40) \
                    or (int(time.strftime("%S", ts)) >50):
                    self.update_light()
            if self.stack:
                if self.stack[0] == "red":
                    self.red()
                    self.miniloop = True
                elif self.stack[0] == "green":
                    self.green()
                    self.miniloop = True
                elif self.stack[0] == "flash":
                    print("flash")
                    self.miniloop = True

                self.stack.pop(0)
            else:
                self.rainbow(self.strip)


#!/usr/bin/env python
from lights import Lights
import RPi.GPIO as GPIO
import SimpleMFRC522

import argparse
import json
import os
import subprocess
import sys
from time import sleep
import urllib
import urllib2
from lights import Lights
import time

# Parse the command line arguments
arg_parser = argparse.ArgumentParser(description='Translates QR codes detected by a camera into Sonos commands.')
arg_parser.add_argument('--default-device', default='Office', help='the name of your default device/room')
arg_parser.add_argument('--default-light', default='Light', help='the name of your default light')
arg_parser.add_argument('--linein-source', default='Dining Room', help='the name of the device/room used as the line-in source')
arg_parser.add_argument('--hostname', default='localhost', help='the hostname or IP address of the machine running `node-sonos-http-api`')
arg_parser.add_argument('--skip-load', action='store_true', help='skip loading of the music library (useful if the server has already loaded it)')
arg_parser.add_argument('--debug-file', help='read commands from a file instead of launching scanner')
args = arg_parser.parse_args()
print args



l1 = Lights()
l1.start()

reader = SimpleMFRC522.SimpleMFRC522()

base_url = 'http://' + args.hostname + ':5005'

base_url_light = 'http://' + args.hostname + ':8080/rest/items'

# Load the most recently used device, if available, otherwise fall back on the `default-device` argument
try:
    with open('.last-device', 'r') as device_file:
        current_device = device_file.read().replace('\n', '')
        print('Defaulting to last used room: ' + current_device)
except:
    current_device = args.default_device
    print('Initial room: ' + current_device)

try:
    with open('.last-light', 'r') as light_file:
        current_light = light_file.read().replace('\n', '')
        print('Defaulting to last used room: ' + current_light)
except:
    current_light = args.default_light
    print('Initial room: ' + current_device)

last_rfidid = ''
lpr = False


class Mode:
    PLAY_SONG_IMMEDIATELY = 1
    PLAY_ALBUM_IMMEDIATELY = 2
    BUILD_QUEUE = 3

current_mode = Mode.PLAY_SONG_IMMEDIATELY

def perform_request(url, data=None, type="GET"):
    print(url)
    if(type=="GET"):
        try:
           response = urllib2.urlopen(url)
           result = response.read()
           print(result)
        except urllib2.HTTPError as e:
            error_message = e.read()
            print error_message
            pass

    elif type=="POST":
      handler=urllib2.HTTPHandler(debuglevel=1)
      opener = urllib2.build_opener(handler)
      urllib2.install_opener(opener)
      request = urllib2.Request(url, data, headers={"Accept":"application/json", "Content-Type": "text/plain"})
      try:
        response = urllib2.urlopen(request)
        result = response.read()
        print(result)
      except urllib2.HTTPError as e:
        error_message = e.read()
        print error_message
        pass

def perform_global_request(path):
    perform_request(base_url + '/' + path)


def perform_room_request(path):
    qdevice = urllib.quote(current_device)
    perform_request(base_url + '/' + qdevice + '/' + path)

def perform_light_request(data):
    qroom = urllib.quote(current_light)
    perform_request(base_url_light+'/'+qroom, data, "POST")

def switch_to_room(room):
    global current_device

    perform_global_request('pauseall')
    current_device = room
    with open(".last-device", "w") as device_file:
        device_file.write(current_device)


def speak(phrase):
    print('SPEAKING: \'{0}\''.format(phrase))
    perform_room_request('say/' + urllib.quote(phrase))


def handle_command(qrcode):
    global current_mode
    global lpr
    print('HANDLING COMMAND: ' + qrcode)
    qrcode=qrcode.strip()
    if qrcode == 'cmd:playpause':
        perform_room_request('playpause')
        phrase = None
    elif qrcode == 'cmd:next':
        perform_room_request('next')
        phrase = None
    elif qrcode == 'cmd:previous':
        perform_room_request('previous')
        phrase = None
    elif qrcode == 'cmd:turntable':
        perform_room_request('linein/' + urllib.quote(args.linein_source))
        perform_room_request('play')
        phrase = 'I\'ve activated the turntable'
    elif qrcode == 'cmd:livingroom':
        switch_to_room('Living Room')
        phrase = 'I\'m switching to the living room'
    elif qrcode == 'cmd:diningandkitchen':
        switch_to_room('Dining Room')
        phrase = 'I\'m switching to the dining room'
    elif qrcode == 'cmd:songonly':
        current_mode = Mode.PLAY_SONG_IMMEDIATELY
        phrase = 'Show me a card and I\'ll play that song right away'
    elif qrcode == 'cmd:wholealbum':
        current_mode = Mode.PLAY_ALBUM_IMMEDIATELY
        phrase = 'Show me a card and I\'ll play the whole album'
    elif qrcode == 'cmd:buildqueue':
        current_mode = Mode.BUILD_QUEUE
        #perform_room_request('pause')
        perform_room_request('clearqueue')
        phrase = 'Let\'s build a list of songs'
    elif qrcode == 'cmd:whatsong':
        perform_room_request('saysong')
        phrase = None
    elif qrcode == 'cmd:whatnext':
        perform_room_request('saynext')
        phrase = None
    elif qrcode == 'cmd:volumeup':
        perform_room_request('volume/+5')
        phrase = None
    elif qrcode == 'cmd:volumedown':
        perform_room_request('volume/-5')
        phrase = None
    elif qrcode == 'cmd:lightblue':
        perform_light_request("237,61,85")
        lpr = False
        phrase = 'Turning lights blue'
        l1.stop_lightparty()
    elif qrcode == 'cmd:lightyellow':
        perform_light_request("60,61,85")
        lpr = False
        phrase = 'Turning lights yellow'
    elif qrcode == 'cmd:lightpink':
        perform_light_request("300,61,85")
        lpr = False
        phrase = 'Turning lights pink'
        l1.stop_lightparty()
    elif qrcode == 'cmd:lightparty':
        light_party()
        phrase = 'Light party!'
    elif qrcode == 'cmd:lightwhite':
        perform_light_request("255, 0, 85")
        lpr = False
        phrase = 'Turning lights white'
        l1.stop_lightparty()
    else:
        phrase = 'Hmm, I don\'t recognize that command'

    if phrase:
        speak(phrase)


def light_party():
    global lpr
    if lpr == False:
        qroom = urllib.quote(current_light)
        l1.start_lightparty(base_url_light+'/'+qroom)
        lpr=True
    else:
        lpr = False
        l1.stop_lightparty()

def handle_library_item(uri):
    if not uri.startswith('lib:'):
        return

    print('PLAYING FROM LIBRARY: ' + uri)

    if current_mode == Mode.BUILD_QUEUE:
        action = 'queuesongfromhash'
    elif current_mode == Mode.PLAY_ALBUM_IMMEDIATELY:
        action = 'playalbumfromhash'
    else:
        action = 'playsongfromhash'

    perform_room_request('musicsearch/library/{0}/{1}'.format(action, uri))


def handle_spotify_item(uri):
    print('PLAYING FROM SPOTIFY: ' + uri)

    if current_mode == Mode.BUILD_QUEUE:
        action = 'queue'
    elif current_mode == Mode.PLAY_ALBUM_IMMEDIATELY:
        action = 'clearqueueandplayalbum'
    else:
        action = 'clearqueueandplaysong'

    perform_room_request('spotify/{0}/{1}'.format(action, uri))

def handle_rfidtag(tag):
    global last_rfidid

    # Ignore redundant codes, except for commands like "whatsong", where you might
    # want to perform it multiple times
    if tag == last_rfidid and not tag.startswith('cmd:'):
      print('IGNORING REDUNDANT QRCODE: ' + tag)
      return

    print('HANDLING QRCODE: ' + tag)

    if tag.startswith('cmd:'):
        handle_command(tag)
    elif tag.startswith('spotify:'):
        handle_spotify_item(tag)
    else:
        handle_library_item(tag)

    # Blink the onboard LED to give some visual indication that a code was handled
    # (especially useful for cases where there's no other auditory feedback, like
    # when adding songs to the queue)
    if not args.debug_file:
        l1.green()

    last_qrcode = tag

# Read from the `debug.txt` file and handle one code at a time.
def read_debug_script():
    # Read codes from `debug.txt`
    with open(args.debug_file) as f:
        debug_codes = f.readlines()

    # Handle each code followed by a short delay
    for code in debug_codes:
        # Remove any trailing comments and newline (and ignore any empty or comment-only lines)
        code = code.split("#")[0]
        code = code.strip()
        if code:
            handle_rfidtag(code)
            sleep(4)

def start_scan():
    while True:
          id2, text = reader.read()
          print(id2)
          print(text)
          handle_rfidtag(text)


perform_room_request('pause')
speak('Hello Barbers!.')

if not args.skip_load:
    # Preload library on startup (it takes a few seconds to prepare the cache)
    print('Indexing the library...')
    speak('Please give me a moment to gather my thoughts.')
    perform_room_request('musicsearch/library/loadifneeded')
    print('Indexing complete!')
    speak('I\'m ready now! Lets have a light party!')

speak('Show me a card!')

if args.debug_file:
    # Run through a list of codes from a local file
    read_debug_script()
else:
    try:
        start_scan()
    except KeyboardInterrupt:
        print('Stopping scanner...')






